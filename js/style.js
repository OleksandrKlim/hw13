"use strict";
let list = document.querySelectorAll(".image-to-show");
let nam = 0;
list[nam].classList.remove("hide");
function hiding() {
  list[nam].classList.add("hide");
  nam = (nam + 1) % list.length;
  list[nam].classList.remove("hide");
  clearTimeout(timer);
  timer = setTimeout(hiding, 3000);
}
let timer = setTimeout(hiding, 3000);
document.body.addEventListener("click", (event) => {
  if (event.target.closest("#stop")) {
    clearTimeout(timer);
  } else if (event.target.closest("#run")) {
    clearTimeout(timer);
    timer = setTimeout(hiding, 3000);
  }
});
